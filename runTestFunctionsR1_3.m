load('Y:\SemiAutomated_Organ_Models\_InesProteomeMapData\Recon2.1\Recon3_and_alike\09.17.09.Recon1.mat');
modelR1 = modelSBML;
[TestSolution(:,1),TestSolutionName] = Test4HumanFctExtv4(modelR1,'all');
load('Y:\Studies\Human\CSBHuman\Recon1.1\FinalRecon2\121114_Recon2betaModel.mat')
modelR2 = modelRecon2beta121114;
[TestSolution(:,2),TestSolutionName] = Test4HumanFctExtv4(modelR2,'all');
load('Y:\SemiAutomated_Organ_Models\_InesProteomeMapData\Recon2.1\Recon3_and_alike\Recon2.v04.mat');
[TestSolution(:,3),TestSolutionName] = Test4HumanFctExtv4(modelR204,'all');
load('Y:\SemiAutomated_Organ_Models\_InesProteomeMapData\Recon2.1\Recon3_and_alike\Recon2_2_biomodel.mat');
modelR22=model; clear model
[TestSolution(:,4),TestSolutionName] = Test4HumanFctExtv4(modelR22,'all');
load('Y:\SemiAutomated_Organ_Models\_InesProteomeMapData\Recon2.1\Recon3_and_alike\2016_07_13_Recon3.mat');
modelR3 = modelRecon3model; clear modelRec*
% set exchange uptake to -1
EX =find(~cellfun(@isempty,strfind(modelR3.rxns,'EX_')));
Sink =find(~cellfun(@isempty,strfind(modelR3.rxns,'sink_')));
EX = [EX ;Sink];
EXL=find(modelR3.lb(EX)<0);
modelR3.lb(EX(EXL))=-1;
% set all other bound to -1000 or 1000
modelR3.lb(find(modelR3.lb<=-100))=-1000;
modelR3.ub(find(modelR3.ub>=100))=1000;
[TestSolution(:,5),TestSolutionName] = Test4HumanFctExtv4(modelR3,'all');

